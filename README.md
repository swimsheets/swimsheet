# README #
###Swim Sheets###
####Mission:####
To streamline the process of recording swim times taken by lane time keepers. Allows each time keeper to input 
the time into a mobile app and these times are then automatically imported into a database that can be viewed online.

### How do I get set up? ###
[Database](https://bitbucket.org/swimsheets/swimsheet/src/126b82df840cc9080b975baffda5ef239d2ebf53/Code%20for%20database/?at=master)
was created through a hosting website called Bluehost
This website has different locations that allowed Hailey to create datatables & upload different php files that allowed the project to run

[App](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/?at=master)
open [SwimSheet:1.xcodeproj](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1.xcodeproj/?at=master)
with Xcode as the workspace while [SwimSheet:1](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/?at=master)
has all the code for the workspace.

### App ###
We used Xcode to write the swift app
Created five pages for the app and a loading screen
Eight different classes

[AppDelegate](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/AppDelegate.swift?at=master&fileviewer=file-view-default): auto generated class to make sure app screen loads 

[ViewController](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/ViewController.swift?at=master&fileviewer=file-view-default): Page One of SwimSheets App, Input Lane Number. Button "go" segue to PageTwo.

[PageTwo](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/PageTwo.swift?at=master&fileviewer=file-view-default): Second page of SwimSheets App with HomeModlel.swift as a protocol. Displays the next input in the php file for the given lane, printing the event name, heat number, team name and the swimmer's first and last name. Then asks for a confirmation for the swimmer's name "Yes" Button segue to PageThree, "No" Button segue to PageFive. 

[PageThree](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/PageThree.swift?at=master&fileviewer=file-view-default): Third page of SwimSheets input time (MM:SS.MsMs). UITextField only allows two digit inputs with the first input being no greater than six to insure valid time. Confirmation for the time "Yes" Button segue to PageFour, "No" Button clears time input by reloading the page.

[PageFour](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/PageFour.swift?at=master&fileviewer=file-view-default): When page is loaded, it sends time input to updateresult.php. Then Displays data from PageTwo ( event name, heat number, team name and the swimmer's first and last name) or if name of swimmer is changed PageFive ( swimmer's first and last name) , and time from page three. Then pick "Next Race" Button segue back to PageTwo to read the next input for the lane number or "Exit" to quit and segue back to ViewController (page one).

[PageFive](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/PageFive.swift?at=master&fileviewer=file-view-default): With HomeModlel.swift as a protocol. Displays all possible swimmer names in table view. The search bar can be used to find and select new swimmers name. After name is picked, "Go" Button segue to PageThree.

[HomeModlel](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/HomeModel.swift?at=master&fileviewer=file-view-default): Connects app to getdata.php. This reads the file and sends each line of data to LocationModel as a jsonElement that can be read by classes with HomeModlel.swift as a protocol (PageTwo and PageFive).

[LocationModel](https://bitbucket.org/swimsheets/swimsheet/src/1202ab42e53017ccfae3346dc93147291a8d7d05/SwimSheet:1/SwimSheet:1/LocationModel.swift?at=master&fileviewer=file-view-default):' Stores all the data sent from HomeModlel one line at a time as an NSObject that can be called and read.

### Database ###
We used Bluehost to set an SQL database and a web domain (swimsheets.com). Used myphpadmin to set up the data table that has columns consisting of: 

ID (an auto incremented number), Team, First Name, Last Name, Event Number, Event Name, Heat Number, Lane Number, Time (this value can be null)

Data is inserted into this table when the coaches go online and download the excel XML file, fill it out, and upload it at swimsheets.com/upload.php based on the instructions listed on swimsheets.com
Created various php files needed to execute our commands using sql queries:

[upload](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/upload.php?at=master&fileviewer=file-view-default): this is where coaches can submit the entries for the meet, once the submit button is hit, the page is directed to swimsheets.com/import.php

[import](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/import.php?at=master&fileviewer=file-view-default): uploads the meet entry files into the datatable 

[getdata](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/getdata.php?at=master&fileviewer=file-view-default): establishes a connection with the SQL database, displays the entries datatable as an array that can be accessed using Xcode in swift 

[updateresult](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/updateresult.php?at=master&fileviewer=file-view-default): takes in the time that is entered in the app and adds it to the entries datatable, if there is a blank time given for a Swimmer, "No Show" will be entered into the time field

[displaybyevent](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/displaybyevent.php?at=master&fileviewer=file-view-default): displays the entries table, sorted by event number and lane number

[displaybyname](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/displaybyname.php?at=master&fileviewer=file-view-default): displays the entries table, sorted alphabetically by last name

[displaybyteam](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/displaybyteam.php?at=master&fileviewer=file-view-default): displays the entries table, sorted by team

[emptydata](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/emptydata.php?at=master&fileviewer=file-view-default): this php codes empties the entries datatable so that a new meet can be started with a clean database

[newmeet](https://bitbucket.org/swimsheets/swimsheet/src/62cbaae0bb10316dd48f44ef94f161df9c15cc5f/Code%20for%20database/newmeet.php?at=master&fileviewer=file-view-default): creates a button that allows the user to empty the database before beginning a new meet
### Who do I talk to? ###
Rebecca Bruce and Hailey Jenkins