
<?php
	$result = "";
	//establish connection to the database
	$con=mysqli_connect("localhost","swimshee_Hailey","Swimming98#","swimshee_database");

	// Check connection
	if (mysqli_connect_error())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
//create a delete all function that will clear the database
	function delete_all()
		{
		    //establish global factors to link between the function & database connection
			global $result;
			global $con;
            
            //delete data from entries if they have an ID, this deletes all entries
			$sql = "DELETE FROM entries Where ID > 0";
			mysqli_query($con, $sql);
            //confirms that the database is cleared
			if ($con->query($sql)===TRUE) 
				{
					$result = "Database Cleared";
				} 
			else 
			//displays if the database fails to clear
				{
					$result = "Error: " .sql. "<br>". $con->error;
				}
			
			mysqli_close($con);
		}

//  Execute the delete_all() function

	delete_all();
?>
<html>
	<body>
		<p>
			Attempt to clear the database:
			<br>
			<br>	
			<?php 
				global $result;
				echo $result; 
			?>
			<br>
		</p>
	</body>
</html>
 