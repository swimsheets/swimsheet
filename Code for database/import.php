<?php
 //create an array to store data
$data = array();
 
 //establish database connection
$con=mysqli_connect("localhost","swimshee_Hailey","Swimming98#","swimshee_database");

// Check connection
if (mysqli_connect_error())
{
echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
 //create an add person function that enters swimmers into the entry table
function add_person( $Team, $FirstName, $LastName, $EventNumber, $EventName, $HeatNumber, $LaneNumber, $Time )
{
    //create objects that link between the function and the database connection
  global $data, $con;
 //sql statement that inserts data from the xml file into the entries datatable, time can be null
 $sql= "INSERT INTO entries ( Team, FirstName, LastName, EventNumber, EventName, HeatNumber, LaneNumber, Time)
 VALUES('$Team', '$FirstName', '$LastName', $EventNumber, '$EventName', $HeatNumber, $LaneNumber, NULL)";
 
 if ($con->query($sql)===TRUE) {
  ;
 } else{
     echo "Error: " .sql. "<br>". $con->error;
 }

//fill the data array with the labels 
 $data []= array(
'Team' => $Team,
'FirstName' => $FirstName,
'LastName' => $LastName,
'EventNumber' => $EventNumber,
'EventName' => $EventName,
'HeatNumber' => $HeatNumber,
'LaneNumber' => $LaneNumber,
'Time' => $Time,
 );
}

//check the uploaded file
if ( $_FILES['file']['tmp_name'] )
{
    //check the file for entries
 $dom = DOMDocument::load( $_FILES['file']['tmp_name'] );
 $rows = $dom->getElementsByTagName( 'Row' );
 $first_row = true;
 foreach ($rows as $row)
 {
   if ( !$first_row )
   {
    $Team = "";
    $FirstName = "";
    $LastName = "";
    $EventNumber = "";
    $EventName = "";
    $HeatNumber = "";
    $LaneNumber = "";
    $Time = "";

     $index = 1;
     $cells = $row->getElementsByTagName( 'Cell' );
     foreach( $cells as $cell )
     {
       $ind = $cell->getAttribute( 'Index' );
            if ( $ind != null ) $index = $ind;
            if ( $index == 1 ) $Team = $cell->nodeValue;
            if ( $index == 2 ) $FirstName = $cell->nodeValue;
            if ( $index == 3 ) $LastName = $cell->nodeValue;
            if ( $index == 4 ) $EventNumber = $cell->nodeValue;
            if ( $index == 5 ) $EventName = $cell->nodeValue;
            if ( $index == 6 ) $HeatNumber = $cell->nodeValue;
            if ( $index == 7 ) $LaneNumber = $cell->nodeValue;
            if ( $index == 8 ) $Time = $cell->nodeValue; 
                $index += 1;
     }
     add_person($Team, $FirstName, $LastName, $EventNumber, $EventName, $HeatNumber, $LaneNumber, $Time);
   }
   $first_row = false;
 }
}
?>
<html>
<body>
<table>
<tr>
<th>Team</th>
<th>FirstName</th>
<th>LastName</th>
<th>EventNumber</th>
<th>EventName</th>
<th>HeatNumber</th>
<th>LaneNumber</th>
<th>Time</th>
</tr>
<?php foreach( $data as $row ) { ?>
<tr>
<td><?php echo( $row['Team'] ); ?></td>
<td><?php echo( $row['FirstName'] ); ?></td>
<td><?php echo( $row['LastName'] ); ?></td>
<td><?php echo( $row['EventNumber'] ); ?></td>
<td><?php echo( $row['EventName'] ); ?></td>
<td><?php echo( $row['HeatNumber'] ); ?></td>
<td><?php echo( $row['LaneNumber'] ); ?></td>
<td><?php echo( $row['Time'] ); ?></td></tr>
<?php } ?>
</table>
</body>
</html>

//code adapted from https://www.ibm.com/developerworks/library/os-phpexcel/index.html
