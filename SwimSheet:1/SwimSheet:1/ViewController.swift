//
//  ViewController.swift
//  SwimSheet

//  Created by Rebecca M. Bruce on 10/13/17.
//  Copyright © 2017 Rebecca M. Bruce. All rights reserved.
//  Uses some code from https://grokswift.com/uitextfield/ is Idenfied as Grok
//  Page One of SwimSheets App. Input Lane Number to start time keeping. When Button go is pushed (function called nextpg) and lane has been entered then transitions to class PageTwo.swift with Lane Number.

import UIKit
class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var lane: UITextField!
    var laneNum = ""
    @IBOutlet weak var problem: UILabel!
    func textField(_ textFieldToChange: UITextField,shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{ //makes sure text input is a number from 1,9. Only takes one input.
        
        let characterCountLimit = 1 //number of inputs (one)  Grok: Character Number Limit
        let startingLength = lane.text?.count ?? 0           // Grok: ''
        let lengthToAdd = string.count                       // Grok: ''
        let lengthToReplace = range.length                   // Grok: ''
            
        let newLength = startingLength + lengthToAdd - lengthToReplace //Grok: ''
            
        let characterSetAllowed = CharacterSet.init(charactersIn: "123456789") //inputs allowed (1-9)
        if let rangeOfCharactersAllowed = string.rangeOfCharacter(from: characterSetAllowed) { //Grok:Only Allow Certain Characters
            let validCharacterCount = string.distance(from: rangeOfCharactersAllowed.lowerBound, to: rangeOfCharactersAllowed.upperBound)         //Grok: Character Number Limit
                if(newLength <= characterCountLimit){
                    return validCharacterCount == string.count
                }
            }
        return false
        }
    
        @IBAction func nextpg(_ sender: UIButton) { //Button "Go" pushed
            if lane.text != "" { //make sure lane number entered
                laneNum = lane.text!
                performSegue(withIdentifier: "showPageTwo", sender: self)
            }
            else{
                problem.text = "Enter Lane Number"
            }
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showPageTwo") {    //send lane number to class PageTwo
            let detailVC = segue.destination as! PageTwo
            detailVC.laneText = laneNum
            print(laneNum)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

