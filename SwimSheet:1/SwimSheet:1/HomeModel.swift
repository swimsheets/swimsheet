//
//  HomeModel.swift
//  SwimSheet:1
//  code for HomeModel is based of code with chris https://codewithchris.com/iphone-app-connect-to-mysql-database/ changes where made to fit the format at the php file. Unless noted all code is from code with chris.(CWC)

import Foundation
import UIKit

protocol HomeModelProtocol: class {
    func itemsDownloaded(items: NSArray)
}
class HomeModel: NSObject {
    weak var delegate: HomeModelProtocol!
    
    var data = Data()
    
    let urlPath: String = "http://swimsheets.com/getdata.php"
    func downloadItems() {
        
        let url: URL = URL(string: urlPath)!
        let defaultSession = Foundation.URLSession(configuration: URLSessionConfiguration.default)
        let task = defaultSession.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print("Failed to download data")
            }else {
                print("Data downloaded")
                self.parseJSON(data!)
            }
            
        }
        
        task.resume()
    }
    func parseJSON(_ data:Data) {
        
        var jsonResult = NSArray()
        
        do{
            print(data.description)
            jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
            
        } catch let error as NSError {
            print("this")
            print(error)
            
        }
        
        var jsonElement = NSDictionary()
        let locations = NSMutableArray()
        
        for i in 0 ..< jsonResult.count
        {
            
            jsonElement = jsonResult[i] as! NSDictionary
            
            let location = LocationModel()
            
            //the following insures none of the JsonElement values are nil through optional binding
            if let ID = jsonElement["ID"] as? String,                //changed to fit php file information style is based of CWC
                let Team = jsonElement["Team"] as? String,
                let FirstName = jsonElement["FirstName"] as? String,
                let LastName = jsonElement["LastName"] as? String,
                let EventNumber = jsonElement["EventNumber"] as? String,
                let EventName = jsonElement["EventName"] as? String,
                let HeatNumber = jsonElement["HeatNumber"] as? String,
                let LaneNumber = jsonElement["LaneNumber"] as? String
                //let Time = jsonElement["Time"] as? String
            {
                
                location.ID = ID                                    //changed to fit php file information style is based of CWC
                location.Team = Team
                location.FirstName = FirstName
                location.LastName = LastName
                location.EventNumber = EventNumber
                location.EventName = EventName
    
                location.HeatNumber = HeatNumber
                location.LaneNumber=LaneNumber
                //location.Time = Time
            }
        
            locations.add(location)
            
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            self.delegate.itemsDownloaded(items: locations)
            
        })
    }

}
//  code for HomeModel is based of code with chris https://codewithchris.com/iphone-app-connect-to-mysql-database/ changes where made to fit the format at the php file. Unless noted all code is from code with chris. (CWC)

