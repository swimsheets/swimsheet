//
//  PageFive.swift
//  SwimSheet:1
//
//  Created by Rebecca M. Bruce on 10/14/17.
//  Copyright © 2017 Rebecca M. Bruce. All rights reserved.
// With HomeModlel.swift as a protocol. Displays all possible swimmer names in table view. The search bar can be used to find and select new swimmers name. After name is picked, "Go" Button segue to PageThree.

import UIKit

class PageFive: UIViewController, UITableViewDataSource, UITableViewDelegate, HomeModelProtocol,UISearchBarDelegate {
   
    var laneText = ""
    var count=0
    var ID=""
    var Team = ""
    var SwimmerN = ""
    var EventNumber = ""
    var EventName = ""
    var HeatNumber = ""
    
    
    @IBOutlet weak var bar: UISearchBar!
    @IBOutlet weak var searchController: UITableView!
    var feedItems: NSArray = NSArray()
    var c=0
    var namesArray = [String]()
    var allNames = [String]()
    var selectedLocation : LocationModel = LocationModel()
     @IBOutlet weak var listTableView: UITableView!

    @IBOutlet weak var name: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        let homeModel = HomeModel()
        homeModel.delegate = self
        homeModel.downloadItems()
        numberNames()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func itemsDownloaded(items: NSArray) {
        feedItems = items
        self.listTableView.reloadData()
    }
    func numberNames(){  //total differnt names
        var numberN=0
        var i=0
        var k=0
        var add=0
        var name=""
        var compName=""
        while(i<feedItems.count){ //co through all posable names
            let item: LocationModel = feedItems[i] as! LocationModel
            name="\(item.FirstName!) \(item.LastName!)"
            while(k<i){ // make sure the name at i does not equal any of the names that came before it
                if(k != i){
                    let secondItem: LocationModel = feedItems[k] as! LocationModel
                    compName="\(secondItem.FirstName!) \(secondItem.LastName!)"
                    if(name==compName){
                        k=i   // end loop (bc k is not less than i)
                        add=1 // if name is the same as one all ready added  add=1
                    }
                }
                k=k+1
            }
            k=0  //reset k
            if(add == 0){  // if and only if name != compName for any value of k<i add name to allNames
                numberN=numberN+1
                add=0
                namesArray.append(name)
                allNames.append(name)
            }
            i=i+1
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(namesArray.count==0){
            numberNames()
            c=c+1
        }
        return namesArray.count //number of objects in table view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //display table view
        let cellIdentifier: String = "BasicCell"
        let myCell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)!

        myCell.textLabel!.text = namesArray[indexPath.row]
        
        return myCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { //seclect object (name)
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!)!
        SwimmerN=((currentCell.textLabel!.text)!) //save slected name
        name.text = "Name: \(SwimmerN)"           // display slected name
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if (segue.identifier == "newNameShow3") { //send information to pagethree
            let detailVC = segue.destination as! PageThree;
            detailVC.laneText = laneText
            detailVC.count=count
            detailVC.ID=ID
            detailVC.Team=Team
            detailVC.SwimmerN=SwimmerN
            detailVC.EventNumber=EventNumber
            detailVC.EventName=EventName
            detailVC.HeatNumber=HeatNumber
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) { //searchText cop to allNames now nameArray = search names
        var i=0
        var add=0
        namesArray.removeAll()
        
        if (searchText.count == 0) { // if searchBar is cleared display all posable names again
            allNames.removeAll()
            numberNames()
        }
        else{
            while (i<allNames.count){
                let range = allNames[i].range(of:searchText, options:.regularExpression)
                if(range != nil) {
                    namesArray.append(allNames[i])  //search through allnames and add only if allNames[i] contanse searchText.
                    add=add+1
                }
                i=i+1
            }
            if(add == 0){ // if searchText is not found in any name
                 namesArray.append("No Names Found try new Input")
            }
        }
        self.listTableView.reloadData() //reset table view 
    }

}
