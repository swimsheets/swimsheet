//
//  PageThree.swift
//  SwimSheet:1
//  Uses some code from https://grokswift.com/uitextfield/ is Idenfied as Grok
//  Created by Rebecca M. Bruce on 10/14/17.
//  reference Simplified iOS "Xcode Text Field Tutorial for iOS Application using Swift" July 28, 2016 by Belal Khan
//  Copyright � 2017 Rebecca M. Bruce. All rights reserved.
//PageThree.swift: Third page of SwimSheets input time (MM:SS.MsMs). UITextField only allows two digit inputs with the first input being no greater than six to insure valid time. Confirmation for the time "Yes" Button segue to PageFour, "No" Button clears time input by reloading the page.

import UIKit
class PageThree: UIViewController, UITextFieldDelegate{
   
    
    var laneText = ""
    var count = 0
    @IBOutlet weak var timeS: UITextField!
    @IBOutlet weak var timeM: UITextField!
    @IBOutlet weak var timeMS: UITextField!
    
    @IBOutlet weak var printTimeM: UILabel!
    @IBOutlet weak var printTimeS: UILabel!
    @IBOutlet weak var printTimeMS: UILabel!
    @IBOutlet weak var problem: UILabel!
    @IBOutlet weak var PrintInfo: UILabel!
    @IBOutlet weak var printSwimmer: UILabel!
    
    @IBOutlet weak var showLane: UILabel!
    
    let textTimeM = "MtextFieldContents"
    let textTimeS = "StextFieldContents"
    let textTimeMS = "MStextFieldContents"
    
    var ID=""
    var Team = ""
    var SwimmerN = ""
    var EventNumber = ""
    var EventName = ""
    var HeatNumber = ""
    
    let defaults = UserDefaults.standard
    
    func textField(_ textFieldToChange: UITextField,shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{ // make sure that the time input is a valid time (2 diget and less than 60)
        
        let characterCountLimit = 2 //Grok: Character Number Limit
        
        if(textFieldToChange == timeM){
            let startingLength = timeM.text?.count ?? 0 // Grok: ''
            let lengthToAdd = string.count              // Grok: ''
            let lengthToReplace = range.length          // Grok: ''
            
            let newLength = startingLength + lengthToAdd - lengthToReplace // Grok: ''
            
            var characterSetAllowed = CharacterSet.decimalDigits // Grok: Only Allow Certain Characters
            if(newLength==1){
                characterSetAllowed = CharacterSet.init(charactersIn: "0123456")
            }
            if let rangeOfCharactersAllowed = string.rangeOfCharacter(from: characterSetAllowed) {
                // make sure it's all of them
                let validCharacterCount = string.distance(from: rangeOfCharactersAllowed.lowerBound, to: rangeOfCharactersAllowed.upperBound) // Grok: Only Allow Certain Characters
                if(newLength <= characterCountLimit){
                    return validCharacterCount == string.count
                }
            }
        }
        if(textFieldToChange == timeS){
            let startingLength = timeS.text?.count ?? 0 // Grok: Character Number Limit
            let lengthToAdd = string.count              // Grok: ''
            let lengthToReplace = range.length          // Grok: ''
            
            let newLength = startingLength + lengthToAdd - lengthToReplace  // Grok: ''
            
            var characterSetAllowed = CharacterSet.decimalDigits // Grok: Only Allow Certain Characters
            if(newLength==1){
                characterSetAllowed = CharacterSet.init(charactersIn: "0123456")
            }
            if let rangeOfCharactersAllowed = string.rangeOfCharacter(from: characterSetAllowed) {
                // make sure it's all of them
                let validCharacterCount = string.distance(from: rangeOfCharactersAllowed.lowerBound, to: rangeOfCharactersAllowed.upperBound)
                if(newLength <= characterCountLimit){
                    return validCharacterCount == string.count
                }
            }
        }
        if(textFieldToChange == timeMS){
            let startingLength = timeMS.text?.count ?? 0 //Grok: Character Number Limit
            let lengthToAdd = string.count               // Grok: ''
            let lengthToReplace = range.length           // Grok: ''
            var characterSetAllowed = CharacterSet.decimalDigits // Grok: Only Allow Certain Characters
            let newLength = startingLength + lengthToAdd - lengthToReplace
            if(newLength==1){
                characterSetAllowed = CharacterSet.init(charactersIn: "0123456")
            }
            if let rangeOfCharactersAllowed = string.rangeOfCharacter(from: characterSetAllowed) {
                // make sure it's all of them, Grok: Only Allow Certain Characters
                let validCharacterCount = string.distance(from: rangeOfCharactersAllowed.lowerBound, to: rangeOfCharactersAllowed.upperBound)
                if(newLength <= characterCountLimit){
                    return validCharacterCount == string.count
                }
            }
            
        }
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {  //send input in textField with enter
        textField.resignFirstResponder()
        return true
    }

    
    @IBAction func tmDone(_ sender: Any) {
        printTimeM.text=timeM.text!
    }
    @IBAction func tsDone(_ sender: Any) {
        printTimeS.text=timeS.text!
    }
    @IBAction func tdone(_ sender: Any) {
        printTimeMS.text=timeMS.text!
    }
    
    
    @IBAction func nextP(_ sender: Any) { //if all times have been input (m,s,ms) then segue else error display
        var count = 0
        
        if printTimeM.text! != "--"{
            count=count+1
        }
        if printTimeS.text! != "--" {
            count=count+1
        }
        if printTimeMS.text! != "--" {
            count=count+1
        }
            
        if(count==3){
            performSegue(withIdentifier: "page4", sender: self)
        }
        else{
            problem.text = "fill out all time filds"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        PrintInfo.text="Event: \(EventName)  Heat: \(HeatNumber)" //display information
        printSwimmer.text="Swimmer \(SwimmerN)"
        showLane.text=laneText
    // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){ //send information to next page
        if (segue.identifier == "page4") {
            let detailVC = segue.destination as! PageFour;
            detailVC.laneText = laneText
            detailVC.count=count
            detailVC.ID=ID
            detailVC.Team=Team
            detailVC.SwimmerN=SwimmerN
            detailVC.EventNumber=EventNumber
            detailVC.EventName=EventName
            detailVC.HeatNumber=HeatNumber
            detailVC.time="\(timeM.text!):\(timeS.text!).\(timeMS.text!)"
        }
    }
    
    
}
