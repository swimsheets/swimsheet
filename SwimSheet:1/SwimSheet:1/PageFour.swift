//
//  PageFour.swift
//  SwimSheet:1
//
//  Created by Rebecca M. Bruce on 10/14/17.
//  Copyright © 2017 Rebecca M. Bruce. All rights reserved.
// When page is loaded, it sends time input to updateresult.php. Then Displays data from PageTwo ( event name, heat number, team name and the swimmer's first and last name) or if name of swimmer is changed PageFive ( swimmer's first and last name) , and time from page three. Then pick "Next Race" Button segue back to PageTwo to read the next input for the lane number or "Exit" to quit and segue back to ViewController (page one).

import UIKit

class PageFour: UIViewController {
    var laneText = ""
    var count=0
    var ID=""
    var Team = ""
    var SwimmerN=""
    var EventNumber = ""
    var EventName = ""
    var HeatNumber = ""
    var time=""
    
    
    @IBOutlet weak var printEvent: UILabel!
    @IBOutlet weak var printHeat: UILabel!
    @IBOutlet weak var team: UILabel!
    @IBOutlet weak var printswimmer: UILabel!
    @IBOutlet weak var printTime: UILabel!
    override func viewDidAppear(_ animated: Bool) {
        let FName=SwimmerN.components(separatedBy:" ")
        let url = URL(string: "http://swimsheets.com/updateresult.php?ID=\(ID)&time=\(time)&FirstName=\(FName[0])")! //url to send name and time
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {                                                 // check for fundamental networking error
                print("error=\(error!)")
                return
            }
        }
    task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        printEvent.text="Event \(EventNumber): \(EventName)"
        printHeat.text="Heat \(HeatNumber)"
        team.text=Team
        printTime.text="Time: \(time)"
        printswimmer.text=SwimmerN
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Next(_ sender: UIButton) {
        performSegue(withIdentifier: "backTo2", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if (segue.identifier == "backTo2") {
            let detailVC = segue.destination as! PageTwo;
            detailVC.laneText = laneText
            print(count)
            detailVC.count=count
        }
    }

}
