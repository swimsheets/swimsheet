//  LocationModel.swift
//  SwimSheet:1
//Code for LocationModel is based of code with chris https://codewithchris.com/iphone-app-connect-to-mysql-database/ changes where made to fit the format off the php file. Unless noted all code is from code with chris.(CWC)
//LocationModel Stores all the data sent from HomeModlel one line at a time as an NSObject that can be called and read.


import Foundation

class LocationModel: NSObject {
    
    var ID:String?  //changed to fit php file information style is based of CWC
    var Team:String?
    var FirstName:String?
    var LastName:String?
    var EventNumber:String?
    var EventName:String?
    var HeatNumber:String?
    var LaneNumber:String?

    override init()
    {
        
    }
    init(ID: String, Team: String, FirstName: String, LastName:String,EventNumber:String,EventName:String,HeatNumber:String,LaneNumber:String){
        //changed to fit php file information style is based of CWC
        self.ID = ID
        self.Team = Team
        self.FirstName=FirstName
        self.LastName=LastName
        self.EventNumber=EventNumber
        self.EventName=EventName
        self.HeatNumber=HeatNumber
        self.LaneNumber=LaneNumber
       
    }
    override var description: String{
        return "swimID: \(ID), FirstName: \(FirstName), LastName: \(LastName),EventNumber: \(EventNumber),EventName \(EventName),HeatNumber: \(HeatNumber),LaneNumber: \(LaneNumber)"
    }
}
//Code for LocationModel is based of code with chris https://codewithchris.com/iphone-app-connect-to-mysql-database/ changes where made to fit the format off the php file. Unless noted all code is from code with chris.(CWC)

