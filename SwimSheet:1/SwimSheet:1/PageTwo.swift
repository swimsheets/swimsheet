//
//  PageTwo.swift
//  SwimSheet:1
//
//  Created by Rebecca M. Bruce on 10/14/17.
//  some code from code with chris https://codewithchris.com/iphone-app-connect-to-mysql-database/ Idenfied as CWC
// Second page of SwimSheets App with HomeModlel.swift as a protocol. Displays the next input in the php file for the given lane, printing the event name, heat number, team name and the swimmer's first and last name. Then asks for a confirmation for the swimmer's name "Yes" Button segue to PageThree, "No" Button segue to PageFive.
import UIKit

class PageTwo: UIViewController, HomeModelProtocol {

    
    @IBOutlet weak var EventPrint: UILabel!
    @IBOutlet weak var HeatPrint: UILabel!
    @IBOutlet weak var Swimmer: UILabel!
    @IBOutlet weak var teamP: UILabel!
    @IBOutlet weak var IDprint: UILabel!
    
    var feedItems: NSArray = NSArray() //CWC
    var selectedLocation : LocationModel = LocationModel() //CWC
    
    var laneText = ""
    var count = 0
    var i=0
    var ID=""
    var Team = ""
    var SwimmerN = ""
    var EventNumber = ""
    var EventName = ""
    var HeatNumber = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let homeModel = HomeModel() //calls homeModel
        homeModel.delegate = self
        homeModel.downloadItems()
        
        
    }
    func itemsDownloaded(items: NSArray) { 
        feedItems = items //CWC
        runTest()
        if(EventNumber==""){
            let alert = UIAlertController(title: "No Event in Lane", message: "Back to Home Page", preferredStyle: UIAlertControllerStyle.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                self.performSegue(withIdentifier: "doneInLane", sender: self)
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    func runTest(){ //looks for next event in given lane
        while(count < feedItems.count) {
            let item: LocationModel = feedItems[count] as! LocationModel //CWC
            if(item.LaneNumber == laneText){ //loops through all lines until the lane given is equal to the lane of the line of text
                ID=item.ID!
                Team=item.Team!
                SwimmerN="\(item.FirstName!) \(item.LastName!)"
                EventNumber=item.EventNumber!
                EventName=item.EventName!
                HeatNumber=item.HeatNumber!
                
                EventPrint.text="Event: \(EventName)"
                HeatPrint.text="Heat Number: \(HeatNumber)"
                Swimmer.text="Swimmer: \(SwimmerN)"
                teamP.text="Team: \(Team)"
                IDprint.text="\(ID)"
                i=count
                count=feedItems.count;
            }
            count=count+1
        }
        count=i+1 // save location + 1 so that next time the page is called the loop will find the next event in the lane
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){ //send data to  pagethree
        if (segue.identifier == "page3") {
            let detailVC = segue.destination as! PageThree;
            detailVC.laneText = laneText
            detailVC.count=count
            detailVC.ID=ID
            detailVC.Team=Team
            detailVC.SwimmerN=SwimmerN
            detailVC.EventNumber=EventNumber
            detailVC.EventName=EventName
            detailVC.HeatNumber=HeatNumber
        }
            if(segue.identifier == "PageFive"){ //send data to next pagefive
                let detailVC = segue.destination as! PageFive;
                detailVC.laneText = laneText
                detailVC.count=count
                detailVC.ID=ID
                detailVC.Team=Team
                detailVC.SwimmerN=SwimmerN
                detailVC.EventNumber=EventNumber
                detailVC.EventName=EventName
                detailVC.HeatNumber=HeatNumber
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
